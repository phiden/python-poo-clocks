class Clock:
    """
    display Clock (int, int) with format "hh:mm"
    """
    def __init__(self, hour, minute):
        self.hour = hour
        self.minute = minute

    def __str__(self):
        minuteTotal = self.hour * 60 + self.minute
        hourDisplayed, minuteDisplayed = divmod(minuteTotal, 60)
        hourDisplayed = hourDisplayed % 24
        # reformat hourDisplayed: example 8 -> '08', 15 -> '15'
        if (hourDisplayed < 10):
            hourDisplayed = '0' + str(hourDisplayed)
        else:
            hourDisplayed = str(hourDisplayed)
        # reformat minuteDisplayed: example 8 -> '08', 15 -> '15'
        if (minuteDisplayed < 10):
            minuteDisplayed = '0' + str(minuteDisplayed)
        else:
            minuteDisplayed = str(minuteDisplayed)
        return hourDisplayed + ':' + minuteDisplayed

    def __add__(self, mins):
        self.minute += mins
        return Clock(self.hour, self.minute)

    def __sub__(self, mins):
        self.minute -= mins
        return Clock(self.hour, self.minute)

    def __eq__(self, otherClock):
        if (self.__str__() == otherClock.__str__()):
            return True
        return False

